import { Component, ViewChild } from "@angular/core";
import { MatDialog, MatTableDataSource, MatSort } from "@angular/material";
import { AddProductModal } from "../components/add-product-modal/add-product-modal";
import { ProductService } from "../services/producs.services";
import Product from "../modals/product";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "tiba-app";
  productList: Array<Product>;

  constructor(
    public dialog: MatDialog,
    private _productService: ProductService
  ) {}

  addProduct() {
    this.openProductModal(new Product());
  }

  openProductModal(originalProduct) {
    const dialogRef = this.dialog.open(AddProductModal, {
      width: "450px",
      data: originalProduct
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == null) return;
      this._productService.addProduct(result);
    });
  }
}
