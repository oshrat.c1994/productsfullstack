import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MaterialModule } from "./material.module";

import { AppComponent } from "./app.component";
import { ProductItemComponent } from "../components/product-item/product-item.component";
import { AddProductModal } from "../components/add-product-modal/add-product-modal";
import { ProductsTableComponent } from "src/components/products-table/products-table.component";

import { ProductService } from "../services/producs.services";
import { LocalStorageService } from "src/services/localStorage.service";

@NgModule({
  entryComponents: [AddProductModal],
  declarations: [
    AppComponent,
    ProductItemComponent,
    AddProductModal,
    ProductsTableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ProductService, LocalStorageService],
  bootstrap: [AppComponent],
  exports: [MaterialModule]
})
export class AppModule {}
