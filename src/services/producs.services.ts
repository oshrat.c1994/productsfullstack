import { Injectable } from "@angular/core";
import * as Rx from "rxjs";
import { LocalStorageService } from "./localStorage.service";
import Product from "../modals/product";

@Injectable()
export class ProductService {
  private productList: Array<Product> = new Array<Product>();
  productListSubject;

  constructor(private _localStorageService: LocalStorageService) {
    this.productList = this._localStorageService.getProductList() || [];
    this.productListSubject = new Rx.BehaviorSubject(this.productList);
  }

  getProductListSubject() {
    return this.productListSubject;
  }

  addProduct(newProuct) {
    this.productList.push({
      ...newProuct,
      id: Date.now().toString(),
      createdDate: Date.now()
    });
    this.productListUpdated();
  }

  editProduct(updatedProduct) {
    let originalProduct = this.productList.find(
      item => item.id == updatedProduct.id
    );
    originalProduct = Object.assign(originalProduct, { ...updatedProduct });
    this.productListUpdated();
  }

  deleteProduct(productId) {
    this.productList = this.productList.filter(
      product => product.id != productId
    );
    this.productListUpdated();
  }

  private productListUpdated() {
    this.productListSubject.next(JSON.parse(JSON.stringify(this.productList)));
    this._localStorageService.saveProductList(this.productList);
  }
}
