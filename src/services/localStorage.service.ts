import { Injectable } from "@angular/core";

@Injectable()
export class LocalStorageService {
  private PRODUCT_LIST_KEY = "productList";
  constructor() {}

  saveProductList(list) {
    localStorage.setItem(this.PRODUCT_LIST_KEY, JSON.stringify(list));
  }

  getProductList() {
    return JSON.parse(localStorage.getItem(this.PRODUCT_LIST_KEY));
  }
}
