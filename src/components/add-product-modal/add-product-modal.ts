import { Component, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import Product from "src/modals/product";

@Component({
  selector: "add-product-modal",
  templateUrl: "add-product-modal.html",
  styleUrls: ["./add-product-modal.css"]
})
export class AddProductModal {
  productForm = new FormGroup({
    name: new FormControl(this.data.name, [
      Validators.required,
      Validators.maxLength(50)
    ]),
    category: new FormControl(this.data.category, Validators.required),
    price: new FormControl(this.data.power, [
      Validators.required,
      Validators.min(0)
    ])
  });
  Categories = ["Sport", "Toys", "Food", "Travel"];
  localProduct: Product;
  constructor(
    public dialogRef: MatDialogRef<AddProductModal>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.localProduct = JSON.parse(JSON.stringify(this.data));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
