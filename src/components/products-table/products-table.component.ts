import { Component, ViewChild } from "@angular/core";
import { MatDialog, MatTableDataSource, MatSort } from "@angular/material";
import { ProductService } from "src/services/producs.services";
import { AddProductModal } from "../add-product-modal/add-product-modal";
import Product from "src/modals/product";

@Component({
  selector: "app-products-table",
  templateUrl: "./products-table.component.html",
  styleUrls: ["./products-table.component.css"]
})
export class ProductsTableComponent {
  productList: Array<Product>;

  constructor(
    public dialog: MatDialog,
    private _productService: ProductService
  ) {}
  displayedColumns = ["name", "category", "price", "createdDate", "deleteBtn"];
  dataSource = new MatTableDataSource(this.productList);
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this._productService.getProductListSubject().subscribe(updatedList => {
      this.productList = updatedList;
      this.dataSource.data = this.productList;
    });
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onProductRowClick(product) {
    this.openProductModal(product);
  }

  openProductModal(originalProduct) {
    const dialogRef = this.dialog.open(AddProductModal, {
      width: "450px",
      data: originalProduct
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == null) return;
      this._productService.editProduct(result);
    });
  }

  onDeleteClick(event, productId) {
    this._productService.deleteProduct(productId);
    if (event) {
      event.stopPropagation();
    }
  }
}
