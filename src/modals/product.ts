export default class Product {
  id: string;
  name: string;
  category: string;
  price: number;
  createdDate: number;
}
